package com.revature.flashcard2.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.revature.flashcard2.bean.FlashCard;
@RestController
public class FlashCardCtrl2 {

	@GetMapping("/fc2")
	public FlashCard fc() {
		System.out.println("fc2 hit");
		return new FlashCard(1,"","");
	}
}
