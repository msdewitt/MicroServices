package com.revature.flashcard.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.revature.flashcard.bean.FlashCard;
@RestController
public class FlashCardCtrl {

	@GetMapping("/home")
	public FlashCard home() {
		return new FlashCard(1, "home-q", "home-a");
	}
	
	/*
	 * FlashCard Service 1 needs info from service 2
	 * 		Using RestTemplates: Springs ways of sending Requests
	 */
	@LoadBalanced
	@Bean
	public RestTemplate buildResttemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.build();
	}
	
	@Autowired
	private RestTemplate restTemplate;
	
	@GetMapping("/getFc2")
	public FlashCard getFc() {
		System.out.println("hit /getFc2");
		FlashCard fc = restTemplate.getForObject("http://flashcard-service-2/fc2", FlashCard.class);
		return fc;
	}
}
