package com.revature.flashcard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
@EnableEurekaClient
@SpringBootApplication
public class FlashcardService1Application {
		/*
 		* First Buisness service: flashcard-service-1
 		* @SpringBootApplication
 		* @EnableEurekaClient
 		* Devtools: listens for changes and restarts the project
 		*/
	public static void main(String[] args) {
		SpringApplication.run(FlashcardService1Application.class, args);
	}
}
